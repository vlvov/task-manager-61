package ru.t1.vlvov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import ru.t1.vlvov.tm.model.Task;

import java.util.List;

@Repository
@Scope("prototype")
public interface TaskRepository extends AbstractUserOwnedRepository<Task> {

    @Nullable
    Task findFirstById(@NotNull final String id);

    void deleteByUserId(@NotNull final String userId);

    void deleteAllByUserId(@NotNull final String userId);

    @Nullable
    List<Task> findAllByUserId(@NotNull final String userId);

    @Nullable
    Task findFirstByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    List<Task> findAllByUserId(@NotNull final String userId, @NotNull final Sort sort);

    @Nullable
    List<Task> findAllByProjectId(@NotNull final String projectId);

    @Nullable
    List<Task> findAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

}
