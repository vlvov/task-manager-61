package ru.t1.vlvov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;

import java.util.List;

@Repository
@Scope("prototype")
public interface ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDTO> {

    @Nullable
    ProjectDTO findFirstById(@NotNull final String id);

    @Nullable
    List<ProjectDTO> findByUserId(@NotNull final String userId);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteAllByUserId(@NotNull final String userId);

    @Nullable
    ProjectDTO findFirstByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    List<ProjectDTO> findByUserId(@NotNull final String userId, @NotNull final Sort sort);

    @Nullable
    List<ProjectDTO> findAll();

}
