package ru.t1.vlvov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.vlvov.tm.api.service.model.ISessionService;
import ru.t1.vlvov.tm.enumerated.CustomSort;
import ru.t1.vlvov.tm.exception.entity.EntityNotFoundException;
import ru.t1.vlvov.tm.exception.field.IdEmptyException;
import ru.t1.vlvov.tm.model.Session;
import ru.t1.vlvov.tm.repository.model.SessionRepository;

import java.util.Collection;
import java.util.List;

@Service
public final class SessionService extends AbstractUserOwnedService<Session> implements ISessionService {

    @Autowired
    @NotNull
    private SessionRepository sessionRepository;

    @Override
    @Transactional
    public void clear() {
        sessionRepository.deleteAll();
    }

    @Override
    @Transactional
    public void add(@Nullable Session model) {
        if (model == null) throw new EntityNotFoundException();
        sessionRepository.save(model);
    }

    @Override
    @Transactional
    public void update(@Nullable Session model) {
        if (model == null) throw new EntityNotFoundException();
        sessionRepository.save(model);
    }

    @Override
    @Transactional
    public void set(@NotNull Collection<Session> collection) {
        clear();
        sessionRepository.saveAll(collection);
    }

    @Override
    @Nullable
    public List<Session> findAll() {
        return sessionRepository.findAll();
    }

    @Override
    @Transactional
    public void remove(@Nullable Session model) {
        if (model == null) throw new EntityNotFoundException();
        sessionRepository.delete(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        sessionRepository.deleteById(id);
    }

    @Override
    @Transactional
    public boolean existsById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return sessionRepository.existsById(id);
    }

    @Override
    public @Nullable Session findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return sessionRepository.findFirstById(id);
    }

    @Override
    @Transactional
    public void add(@Nullable String userId, @Nullable Session model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        sessionRepository.save(model);
    }

    @Override
    @Transactional
    public void update(@Nullable String userId, @Nullable Session model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        sessionRepository.save(model);
    }

    @Override
    @Transactional
    public void clear(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        sessionRepository.deleteAllByUserId(userId);
    }

    @Override
    @Nullable
    public List<Session> findAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return sessionRepository.findAllByUserId(userId);
    }

    @Override
    public @Nullable Session findOneById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return sessionRepository.findFirstByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void remove(@Nullable String userId, @Nullable Session model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        sessionRepository.deleteByUserIdAndId(userId, model.getId());
    }

    @Override
    @Transactional
    public void removeById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        Session model = findOneById(userId, id);
        remove(userId, model);
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return findOneById(userId, id) != null;
    }

    @Override
    @Nullable
    public List<Session> findAll(@Nullable String userId, @Nullable CustomSort sort) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (sort == null) return findAll(userId);
        final Sort sortable = Sort.by(Sort.Direction.ASC, CustomSort.getOrderByField(sort));
        return sessionRepository.findAllByUserId(userId, sortable);
    }

}
