package ru.t1.vlvov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.request.ProjectChangeStatusByIdRequest;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.event.ConsoleEvent;
import ru.t1.vlvov.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class ProjectChangeStatusByIdListener extends AbstractProjectListener {

    @NotNull
    private final String DESCRIPTION = "Change project status by Id.";

    @NotNull
    private final String NAME = "project-change-status-by-id";

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@projectChangeStatusByIdListener.getName() == #event.name")
    public final void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusValue = TerminalUtil.nextLine();
        @Nullable final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(getToken());
        request.setProjectId(id);
        request.setStatus(statusValue);
        projectEndpoint.changeProjectStatusById(request);
    }

}
