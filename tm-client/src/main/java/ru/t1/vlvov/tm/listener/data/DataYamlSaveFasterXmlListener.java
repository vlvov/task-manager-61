package ru.t1.vlvov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.request.DataYamlSaveFasterXmlRequest;
import ru.t1.vlvov.tm.event.ConsoleEvent;

@Component
public final class DataYamlSaveFasterXmlListener extends AbstractDataListener {

    @NotNull
    private final String DESCRIPTION = "Save data to yaml file.";

    @NotNull
    private final String NAME = "data-save-yaml";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@dataYamlSaveFasterXmlListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA SAVE YAML]");
        @NotNull DataYamlSaveFasterXmlRequest request = new DataYamlSaveFasterXmlRequest(getToken());
        domainEndpoint.saveDataYamlFasterXml(request);
    }

}