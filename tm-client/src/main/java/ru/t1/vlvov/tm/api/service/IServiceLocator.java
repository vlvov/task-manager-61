package ru.t1.vlvov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.endpoint.*;

public interface IServiceLocator {
    
    @Nullable
    ILoggerService getLoggerService();

    @Nullable
    ITokenService getTokenService();

    @NotNull
    IDomainEndpoint getDomainEndpoint();

    @NotNull
    IAuthEndpoint getAuthEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

}